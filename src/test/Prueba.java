package test;

import weka.classifiers.trees.Id3;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Prueba { 

    public static void main(String[] args)throws Exception{
        DataSource ds = new DataSource("src/test/BDONCOSALUD.arff");
        Instances ins = ds.getDataSet();
        ins.setClassIndex(14);
//        //NATIVE BAYES
//        NaiveBayes nb = new NaiveBayes();
//        nb.buildClassifier(ins);
//        System.out.println(nb.toString());
//        //RANDOM TREE
//        RandomTree rt = new RandomTree();
//        rt.buildClassifier(ins);
//        System.out.println(rt.toString());
//        //J48 
//        J48 j48 = new J48();
//        j48.buildClassifier(ins);
//        System.out.println(j48.toString());     
        //ID3
        Id3 id3 = new Id3();
        id3.buildClassifier(ins);
        System.out.println(id3.toString());   
        
    }
}
